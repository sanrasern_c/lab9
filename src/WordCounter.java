import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Scanner;
/**
 * This class is to count word and syllables.
 * @author Sanrasern 5710547247
 * 
 */
public class WordCounter {
	State state;
	private int count = 0;
	protected int totalSyllables = 0;

	/**
	 * To count syllables from word.
	 * @param word is word that going to find out.
	 * @return the number of syllables.
	 */
	public int countSyllables (String word) {
		count = 0;
		state = startState;
		for ( char c : word.toCharArray() ) {
			state.handleChar(c);
		}
		if(state == dashState) {
			count = 0;
		}
		else if( state == eState && count != 1) {
			count--;
		}
		return count;
	}
	
	/**
	 * To check that is it a vowel.
	 * @param c is char that going to check.
	 * @return true if c is  a character , false if is not.
	 */
	public boolean isVowel (char c) {
		String vowelList = "aeiouAEIOU";
		String temp = c+"";
		return vowelList.contains(temp);

	}

	/**
	 * Count all words read from an input stream an return the total count.
	 * @param instream is the input stream to read from.
	 * @return number of words in the InputStream.  -1 if the stream
	 *        cannot be read.
	 */	
	public int countWords( URL url ) {
		int newCount =0 ;
		try{
			InputStream in = url.openStream();
			BufferedReader breader = new BufferedReader(new InputStreamReader(in ));
			while( true ) {
				String line = breader.readLine();
				if( line == null ) break;
				totalSyllables += countSyllables(line);
				newCount++;
			}
		}catch (IOException e) {
		}
		return newCount;
	}
	
	/**
	 * This is the first state that first character of the word is in. 
	 */
	State startState = new State() {
		public void handleChar ( char c ) {
			if(c=='e' || c=='E') {
				state = eState;
				count++;
			}
			else if ( isVowel(c) || c=='y' || c=='Y' ){
				state = vowelState;
				count++;
			}
			else if( Character.isLetter(c) )
				state = consonantState;
			else
				state = nonWordState;
		}
	};

	/**
	 * This is the state that when character is a consonant.
	 */
	State consonantState = new State() {
		public void handleChar (char c) {
			if( c=='e' || c=='E' ){
				count++;
				state = eState;
			}
			else if ( isVowel (c) || c=='y' || c=='Y' ){
				count++;
				state = vowelState;
			}
			else if (Character.isLetter(c) )
				state = consonantState;
			else if ( c == '-' ) 
				state = dashState;
			else if (c=='\'')
				state = consonantState;
			else
				state = nonWordState;
		}
	};
	
	/**
	 * This is the state when character is vowel.
	 */
	State vowelState = new State() {
		public void handleChar ( char c ) {
			if( isVowel(c)) 
				state = vowelState;
			else if( Character.isLetter(c) && !isVowel(c) ) 
				state = consonantState;
			else if ( c=='-' )
				state = dashState;
			else 
				state = nonWordState;
		}
	};

	/**
	 * This is a state when character is e . 
	 */
	State eState = new State() {
		public void handleChar (char c) {
			if( isVowel(c) )
				state = vowelState;
			else if( Character.isLetter(c) )
				state = consonantState;
			else if( c=='-' )
				state = dashState;
			else
				state = nonWordState;

		}
	};

	/**
	 * This is s state when found dash.
	 */
	State dashState = new State() {
		public void handleChar (char c) {
			if( isVowel(c) ) {
				count++;
				state = vowelState;
			}
			else if( Character.isLetter(c) )
				state = consonantState;
			else
				state = nonWordState;
		}
	};
	
	/**
	 * This is a state when we found that word is non-word.
	 */
	State nonWordState = new State() {
		public void handleChar (char c) {
			count = 0;
		}
	};

}


