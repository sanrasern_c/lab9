import java.net.MalformedURLException;
import java.net.URL;

/**
 * To run the program.
 * @author Sanrasern 5710547247
 *
 */
public class Main {
	public static void main ( String [] args ) {
		WordCounter wordCounter = new WordCounter();
		StopWatch time = new StopWatch(); 
		final String DICT_URL = "http://se.cpe.ku.ac.th/dictionary.txt";
		try{
			URL url = new URL ( DICT_URL );
			time.start();
			System.out.println(wordCounter.countWords(url));
			time.stop();
			System.out.println(wordCounter.totalSyllables);
			System.out.println(time.getElapsed());
		} catch (MalformedURLException e) {
			
		}
	}
}
