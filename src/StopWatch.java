

/**
 * To create watch that can timing the elapse time.
 * @author Sanrasern Chaihetphon
 */
public class StopWatch {
	private long initTime;
	private long endTime;
	private boolean check;
	private static final double NANOSECONDS = 1.0E-9;

	/**
	 * Constructor for a stopwatch.
	 */
	public StopWatch(){
		check = false;
		initTime = 0;
		endTime = 0;
	}

	/**
	 * To start timing.
	 */
	public void start(){
		if(!isRunning()){
			initTime = System.nanoTime();
			check = true;
		}
	}

	/**
	 * To stop timing.
	 */
	public void stop(){
		if(isRunning()){
			endTime = System.nanoTime();
			check = false;
		}

	}

	/**
	 * To get elapsed time.
	 * @return elapsed time in seconds.
	 */
	public double getElapsed(){
		if(isRunning()) {
			return NANOSECONDS*(System.nanoTime() - initTime);
		}
		else {
			return NANOSECONDS*(endTime - initTime);
		}
	}
	/**
	 * To check that stopwatch is running or not.
	 * @return true if stopwatch is running.
	 */
	public boolean isRunning(){
		return check;
	}
}
